package ASCIIart

import scala.::

class ASCIIart(art: String) {

  val (largeur, hauteur, liste) = {
    val f = art.split('\n')
    (f(0).toInt, f(1).toInt, f.drop(2).toList)
  }

  lazy val tableLettre: Map[Char, List[String]] = listeToMap()

  // question 1 : renvoie la Map qui associe chaque lettre à son ascii art
  // attention : si la lettre n'est pas définie, il fait renvoyer l'ascii-art du symbole ? fourni en derniere position
  private def listeToMap(): Map[Char, List[String]] = {
    def retrievelisteToMap(map: Map[Char, List[String]], index: Int): Map[Char, List[String]] = {

      def retrieveAt(letter: List[String], height: Int): List[String] = {
        if (height >= 0) {
          retrieveAt(liste(height).substring(index*largeur, index*largeur + largeur) :: letter, height - 1)
        } else {
          letter
        }
      }
      if(index < 26){
        retrievelisteToMap(map + (('a' + index).toChar -> retrieveAt(List(), hauteur-1)), index + 1)
      }else{
        map withDefaultValue(retrieveAt(List(), hauteur-1));
      }
    }
    retrievelisteToMap(Map(), 0);
  }

  // question 2 : renvoie le mot sous forme d'ascii art
  def apply(mot: String): String = {
    def listToString(letter :Map[Char,List[String]],i:Int,resultat:String,Height:Int):String = {
      if(i < mot.length){
        listToString(letter,i+1,resultat+letter(mot(i))(Height),Height)
      }else{
        resultat
      }
    }
    def applyLetter(result:String,mot: List[Char],index:Int): String = {
      if(index < hauteur){
        applyLetter(result + listToString(tableLettre,0,"",index) + "\n",mot,index+1)
      }else{
        result.substring(0, result.length() - 1);
      }
    }
    applyLetter("",mot.toList,0)
  }


}
