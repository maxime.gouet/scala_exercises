package object Parentheses {

  // Écrire une fonction récursive qui indique si une phrase dispose de parenthèses bien construite
  def equilibre(phrase: String): Boolean = {
    def verif[T](list : List[T],expectedTime:Int): Boolean ={
      if(!list.isEmpty){
        if(list.head == '('){
          verif(list.tail,expectedTime+1)
        }else if(list.head == ')'){
          if(expectedTime > 0){
            verif(list.tail,expectedTime-1)
          }else{
            false
          }
        }else{
          verif(list.tail,expectedTime)
        }
      }else{
        if(expectedTime == 0){
          true
        }else{
          false
        }
      }
    }
    verif(phrase.toList,0)
  }

  // pareil, mais générique
  def equilibreGenerique(co: Char, cf: Char)(phrase: String): Boolean = {
    def verif[T](list : List[T],expectedOpening: Char,expectedClose: Char,expectedTime:Int): Boolean ={
      if(!list.isEmpty){
        if(list.head == expectedOpening){
          verif(list.tail,expectedOpening,expectedClose,expectedTime+1)
        }else if(list.head == expectedClose){
          if(expectedTime > 0){
            verif(list.tail,expectedOpening,expectedClose,expectedTime-1)
          }else{
            false
          }
        }else{
          verif(list.tail,expectedOpening,expectedClose,expectedTime)
        }
      }else{
        if(expectedTime == 0){
          true
        }else{
          false
        }
      }
    }
    verif(phrase.toList,co,cf,0)
  }

  // utiliser la fonction générique pour définir la version xml avec < et > comme caractère ouvrant/fermant
  lazy val equilibreXml: String => Boolean = equilibreGenerique('<','>')

}
