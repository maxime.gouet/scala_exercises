package Conway

class Conway(init: Int = 1) {

  // renvoie le rang suivant : List(1, 1) --> List(2, 1)
  def lire(rang : List[Int]) : List[Int] = {
    def recursiveReading(suite :List[Int],next:List[Int]): List[Int]={
      if(suite.length > 0){
        val (a,b) = suite.span(_ == suite.head)
        recursiveReading(b,next :+ a.length :+ a.head)
      }else{
        next
      }
    }
    recursiveReading(rang,List())
  }

  // la suite infinie de tout les rangs
  val rangs : LazyList[List[Int]] = {
    def Loop(list:List[Int]):LazyList[List[Int]] = list #:: Loop(lire(list))
    Loop(List(init))
  }

  //renvoie le rang sous forme de chaine de caractère
  // attention : rang commence à 1
  def apply(rang: Int): String = rangs(rang-1).mkString(" ")

}
