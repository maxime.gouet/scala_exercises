package Graphe

import scala.annotation.tailrec

case class Noeud(nom: String)

case class Arc(extremite1: String, extremite2: String)

case class Graphe(noeuds: Set[Noeud], arcs: Set[Arc]) {

  def +(arc: Arc): Graphe =
    Graphe(
      noeuds+Noeud(arc.extremite1)+Noeud(arc.extremite2),
      arcs + arc
    )

  def +(autre: Graphe): Graphe = {
    Graphe(noeuds.union(autre.noeuds),arcs.union(autre.arcs))
  }

  def voisins(noeud: Noeud): Set[Noeud] = {
    arcs.filter(a => a.extremite1 == noeud.nom).map(a => Noeud(a.extremite2)) union arcs.filter(a=> a.extremite2 == noeud.nom).map(a => Noeud(a.extremite1))
  }

  def degre(noeud: Noeud): Int = voisins(noeud).size

  def distance(depart: Noeud, arrive: Noeud): Option[Int] = {
    def DepthSearch(currentNodes: Set[Noeud], dist:Int): Option[Int] = {
      val temp = currentNodes++currentNodes.flatMap(voisins)
      if(temp.contains(arrive)){
        Some(dist)
      }else if(currentNodes == temp){
        None
      }else{
        DepthSearch(temp,dist+1)
      }
  }
    DepthSearch(Set(depart),1)
  }

  lazy val composantesConnexes: Set[Set[Noeud]] = ???

  lazy val estBicoloriable: Boolean = ???

}
