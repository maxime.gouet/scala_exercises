import scala.::

package object Listes {

  // définir une méthode qui renvoie le dernier élément d'une liste s'il existe
  // s'il n'existe pas, on génére une exception NoSuchElementException
  @throws[NoSuchElementException]
  def dernier[T](liste: List[T]): T = {
    def last[T](liste: List[T]): T={
      if(liste.tail.isEmpty)
        liste.head
      else
        last(liste.tail)
    }
    if(liste.isEmpty)
      throw new NoSuchElementException()
    else
      last(liste)

  }

  // définir une méthode qui renvoie le k-ieme élément d'une liste, en commençant par 0, s'il existe
  // s'il n'existe pas, on génére une exception IndexOutOfBoundsException
  @throws[IndexOutOfBoundsException]
  def kieme[T](liste: List[T], k: Int): T = {
    def at[T](liste: List[T],k: Int,i: Int): T= {
      if(liste.isEmpty)
        throw new IndexOutOfBoundsException()
      else {
        if(i == k)
          liste.head
        else
          at(liste.tail,k,i+1)
      }
    }
    at(liste,k,0)
  }

  // définir une méthode qui renvoie le nombre d'élément de la liste
  def taille[T](liste: List[T]): Int = {
    def size[T](liste: List[T],i: Int): Int = {
      if(liste.isEmpty)
        i
      else
        size(liste.tail,i+1)
    }
    size(liste,0)
  }

  // définir une méthode qui renvoie true si l'élément x est présent dans la liste
  def contient[T](liste: List[T], x: T): Boolean = {
    def contain[T](liste: List[T],x: T): Boolean = {
      if(liste.isEmpty)
        false
      else {
        if(x == liste.head)
          true
        else
          contain(liste.tail,x)
      }
    }
    contain(liste,x)
  }

  // définir une méthode qui renvoie une nouvelle liste avec le k-ième élement supprimé
  // si k <= 0, on supprime le premier élément
  // si k>= taille, on renvoie la même liste
  def supprimerKieme[T](liste: List[T], k: Int): List[T] = {
    def remove(num: Int, list: List[T],i: Int): List[T] = {
      if(num == i) {
        list.tail
      } else {
          list.head :: remove(num,list.tail,i+1)
      }
    }
    if(k <= 0){
      liste.tail
    }else if( k >= taille(liste))
      liste
    else
      remove(k,liste,0)
  }

  // définir une méthode qui renvoie une nouvelle liste avec l'élément e en k-ième position
  // si k <= 0, on l'ajoute en premier élément
  // si k>= taille, on l'ajoute en dernier
  def ajouterKieme[T](liste: List[T], k: Int, e: T): List[T] = {
    def add(num: Int, list: List[T],i: Int,e:T): List[T] = {
      if(list.isEmpty){
        e :: Nil
      }else{
        if(num == i) {
          e :: list
        } else {
          list.head :: add(num,list.tail,i+1,e)
        }
      }
    }
    if(k <= 0){
      e :: liste
    }else
      add(k,liste,0,e)
  }

  // définir une méthode qui renvoie true si les deux listes sont identiques
  def identique[T](liste1: List[T], liste2: List[T]): Boolean = {
    def test[T](liste11:List[T],liste21: List[T]):Boolean = {
      if(liste11.isEmpty && !liste21.isEmpty){
        false
      }else if (!liste11.isEmpty && liste21.isEmpty){
        false
      }else if (liste11.isEmpty && liste21.isEmpty){
        true
      }else{
        if(liste11.head == liste21.head)
          test(liste11.tail,liste21.tail)
        else
          false
      }
    }
    test(liste1,liste2)
  }

  // définir une méthode qui renvoie une nouvelle liste en ne gardant que les éléments qui renvoie true au prédicat
  def filtrer[T](liste: List[T], predicat: T => Boolean): List[T] = {
    def filtre(list: List[T], pred: T => Boolean): List[T] ={
      if(!list.isEmpty){
        if(pred(list.head)){
          list.head :: filtre(list.tail,pred)
        }else{
          filtre(list.tail,pred)
        }
      }else{
        Nil
      }
    }
    filtre(liste,predicat)
  }

  // définir une méthode qui renvoie une nouvelle liste où les éléments sont les résultats de f
  def image[T, U](liste: List[T], f: T => U): List[U] = {
    def imag[T,U](list: List[T], z: T => U): List[U] = {
      if(!list.isEmpty){
        z(list.head) :: imag(list.tail,z)
      }else{
        Nil
      }
    }
    imag(liste,f)
  }


}
